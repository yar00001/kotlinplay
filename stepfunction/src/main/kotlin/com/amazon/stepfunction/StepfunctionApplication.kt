package com.amazon.stepfunction

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StepfunctionApplication

fun main(args: Array<String>) {
    runApplication<StepfunctionApplication>(*args)
}
